import pb from '../lib/pocketbase';
import { useForm } from 'react-hook-form';
import useLogout from '../hooks/useLogout';
import useLogin from '../hooks/useLogin';
import useVerified, { requestVerification } from '../hooks/useVerified';

export default function Auth() {
    const { logout } = useLogout();
    const { mutate: login, isLoading, isError } = useLogin();
    const {data: isVerified} = useVerified();
    const { register, handleSubmit, reset } = useForm();

    const IsLoggedIn = pb.authStore.isValid

    const onSubmit = async (data: any) => {
        login({ email: data.email, password: data.password });
        reset();
    }

    if (IsLoggedIn) {
        return <>
            <h1>Love Cherrie so much!!! </h1>
            {isVerified ? <h1>Verified</h1> : <h1>Not Verified</h1>}
            {isVerified ? <></>: <button onClick={requestVerification}>Request Verification</button>}
            <button onClick={logout}>Logout</button>
        </>
    }

    return (
        <>
            {isLoading && <h1>Loading...</h1>}
            {isError && <h1>Invalid username & password</h1>}
            <h1>Please Log In</h1>

            <form onSubmit={handleSubmit(onSubmit)}>
                <input type="email" placeholder='email' {...register('email')} />
                <input type="password" placeholder='password' {...register('password')} />

                <button type="submit" disabled={isLoading}>{isLoading ? 'Loading...' : 'Login'}</button>
            </form>
        </>
    )

}