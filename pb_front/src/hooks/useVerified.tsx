import pb from "@/lib/pocketbase";
import { useQuery } from "react-query";


export default function useVerified() {
    const id = pb.authStore.model?.id;


    const verify = async () => {
        const {verified} = await pb.collection('users').getOne(id);
        return verified;
    }

    return useQuery({queryFn: verify, queryKey: ['verify', id]});
}

export const requestVerification = async () => {
    const email = pb.authStore.model?.email;
    const res = await pb.collection('users').requestVerification(email);
    if (res) {
        alert('Please check your email to verify your account');
    }
}
